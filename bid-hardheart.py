#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from redeal import *
from redeal import dds
from redeal.global_defs import Seat, Suit, Card, Rank, Strain

Deal.set_str_style('long')
Hand.set_str_style('short')

## 开始设定拿牌情况

# 双有
vuln = True
# 坐北，单手牌 H = Hand
predeal = {Seat['N']: H('AK52 8 AK97 A976')}
dealer = Deal.prepare(predeal)

# 这是为了显示
bid="""
叫牌: (双有)
  西   北   东   南
            3S   Pass
  Pass  ?
你坐北，如何在 Pass/3NT/5C/5D 中取舍？
"""

print(bid)
print("你做北，拿牌如下:" , predeal[Seat['N']])

"""
S没有AK，应该肯定要七张六张不考虑 (ok)
不能有边花A (ok)

七张应该至少是 QJTXXXX八张可以是 QJXXXXXX这两种情况，边花至少还要有1.5个赢墩 
或者有边花四张套

7321的话，S要QJT9XXX反正有局3S一般要6.5墩以上
"""

# 额外牌力判断: AKQJT 为 22111
w2q = Evaluator(2, 2, 1, 1, 1)

# 发牌条件，不满足就返回 False
# hcp: 点力

def accept(deal):
    # 先看东家
    e = deal.east  # 简化下面程序
    # 东家点力5-10点
#    if 'A' not in str(e.spades):
#        return False
    if e.hcp <5 or deal.east.hcp >10:
        return False
    # 东家黑桃7张以上 (稍作简化，AK 6张套不管)
    if len(e.spades) < 7:
        return False
    # 不能有边花 A
    if ('A' in e.hearts) or ('A' in e.diamonds) or ('A' in e.clubs):
        return False
    # 东家黑桃7张的话, 应该至少是 QJTXXXX => w2q >= 3
    if len(e.spades) == 7:
        #and w2q(s.spades) > 3  
        if w2q(e.spades) < 3:
            return False
#    print(e.spades, w2q(e.spades))
    # 东家黑桃8张的话, 应该至少是 QJxxxxxx => w2q >= 2 或者 A,K,Q 存在一个 (逻辑有点绕)
    if len(deal.east.spades) >= 8:
        if w2q(e.spades) < 2:
            return False
        if w2q(e.spades) == 2 and ('A' not in e.spades) and ('K' not in e.spades) and ('Q' not in e.spades):
            return False
#    if 'A' not in e.spades:
#        return False

    # 其他花色不能超过4个
    if len(e.hearts) > 4:
        return False
    if len(e.clubs) > 4:
        return False
    if len(e.diamonds) > 4:
        return False
    # 西家点力不能超过18点
    if deal.west.hcp >18:
        return False
    return True

imps_payoff = Payoff(('3S', '3NT', '5C','5D'), imps)

found = 0
n = 1000

deal_1 = deal_2 = deal_3 = deal_4 = None

for _ in range(5000 * n):
    if found > n:
        break
    #deal = dealer(accept)
    deal = dealer()
    if not accept(deal):
        continue
    found += 1
    score_3s = deal.dd_score('3SE', vuln)
    score_3n = deal.dd_score('3NN', vuln)
    score_5c = deal.dd_score('5CS', vuln)
    score_5d = deal.dd_score('5DS', vuln)


    if score_3s > 0:
        deal_1 = deal
    if score_3n > 0:
        deal_2 = deal
    if score_5c > 0:
        deal_3 = deal
    if score_5d > 0:
        deal_4 = deal
    data = {
        '3NT': score_3n,
        '3S': score_3s,
        '5C': score_5c,
        '5D': score_5d
    }
    imps_payoff.add_data(data)

print("模拟分析结果:", str(found))

imps_payoff.report()

print("3S的牌例:", deal_1)
print("3NT牌例:", deal_2)
print("5C牌例:", deal_3)
print("5D牌例:", deal_4)
