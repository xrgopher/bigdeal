#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from redeal import *
from redeal import dds
from redeal.global_defs import Seat, Suit, Card, Rank, Strain

# http://datadaydreams.com/posts/a-simulation-tutorial-for-better-decisionmaking-at-bridge/
# 这是为了显示，看看首攻
bid="""
叫牌: (双无)
  西   北   东   南
      Pass Pass 1NT
 Pass 3NT
你坐西，如何找到最佳首攻？
假设
 南: 15-17点，5332，4432，4333
 北：9-11点，黑桃红桃小于4张，因为没有转移叫和斯台曼 (如果北家点数再高，击败可能性很低不用考虑)

"""

predeal = {Seat['W']: H('J86 95 KQ72 KJ72')}
dealer = Deal.prepare(predeal)

print(bid)
print("你坐西，对抗3NT，拿牌如下，如何首攻:" , predeal[Seat['W']])

# 北家牌的模拟
def accept(deal):
    balanced = Shape("(4333)") + Shape("(4432)") + Shape("(5332)")
    if deal.north.hcp > 12 or deal.north.hcp<10:
        return False
    if len(deal.north.hearts)>4 or len(deal.north.spades)>4:
        return False
    if deal.south.hcp > 17 or deal.south.hcp<15
        return False
    if deal.north.hearts not balanced
        return False
    return True

accept_north = accept

contract = Contract.from_str('3NS')

# https://github.com/anntzer/redeal/blob/master/redeal/dds.py#L135
# 这个应该是固定的
lead_payoff = Payoff(
    #
    # def valid_cards(deal, strain, leader):
    # strain: 定约花色或NT
    # leader: 首攻者
    sorted(dds.valid_cards(dealer(), 'N', 'W'), reverse=True),
    lambda ti, tj: imps(contract.score(ti), contract.score(tj))
)

found = 0
n = 500
for _ in range(1000*n):
    if found > n:
        continue
    deal = dealer()
    if not accept_north(deal):
        continue
    score_3NS = deal.dd_score('3NS')
    if score_3NS < 0:
        found+=1

    # 是否可以挪到accpt函数
    '''if deal.south.hcp < 16:
        continue
    
    # 这段最复杂，慢慢理解
    has_ace = lambda hand: Rank['A'] in hand #lambda是匿名函数（
    n_aces_west = sum(map(int, map(has_ace,
        [deal.west.spades, deal.west.hearts, deal.west.diamonds, deal.west.clubs]
    )))#map是返回前面一个参数对后面的值，所以这行是求解四个花色A的个数
    # 西家有两个A不可能去除
    if n_aces_west > 1:
        continue'''

    # https://github.com/anntzer/redeal/blob/master/redeal/redeal.py#L347
    # 统计双明手东家拿到的墩数
    lead_payoff.add_data(deal.dd_all_tricks('N', 'W'))

print("模拟分析结果 (xx)为上下误差:", found)

lead_payoff.report()
