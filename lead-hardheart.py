#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from redeal import *
from redeal import dds
from redeal.global_defs import Seat, Suit, Card, Rank, Strain

# http://datadaydreams.com/posts/a-simulation-tutorial-for-better-decisionmaking-at-bridge/
# 这是为了显示，看看首攻
bid="""
叫牌: (双无)
  西   北   东   南
       3C   Pass 6C
  Pass Pass
你坐东，如何找到最佳首攻？
"""

predeal = {Seat['E']: H('92 KQT K97654 42')}
dealer = Deal.prepare(predeal)

print(bid)
print("你做东，对抗6C，拿牌如下，如何首攻:" , predeal[Seat['E']])

# 北家牌的模拟
def accept(deal):
    if deal.north.hcp > 10:
        return False
    if len(deal.north.clubs) not in [6, 7]:
        return False
    if deal.north.clubs.hcp < 3:
        return False
    return True

accept_north = accept

contract = Contract.from_str('6CN')

# https://github.com/anntzer/redeal/blob/master/redeal/dds.py#L135
# 这个应该是固定的
lead_payoff = Payoff(
    #
    # def valid_cards(deal, strain, leader):
    # strain: 定约花色或NT
    # leader: 首攻者
    # 'C': 将牌花色
    # 'E': 首攻方
    # imp: 算分内置函数
    sorted(dds.valid_cards(dealer(), 'C', 'E'), reverse=True),
    lambda ti, tj: imps(contract.score(ti), contract.score(tj))
)

found = 0
n = 500
for _ in range(1000*n):
    if found > n:
        continue
    deal = dealer()
    if not accept_north(deal):
        continue
    # 是否可以挪到accpt函数
    if deal.south.hcp < 16:
        continue
    
    # 这段最复杂，慢慢理解
    # 这个时候定义的函数返回值的布尔值，True or False
    has_ace = lambda hand: Rank['A'] in hand
    n_aces_west = sum(map(int, map(has_ace,
        [deal.west.spades, deal.west.hearts, deal.west.diamonds, deal.west.clubs]
    )))
    # 西家有两个A不可能去除
    if n_aces_west > 1:
        continue

    found += 1
    # https://github.com/anntzer/redeal/blob/master/redeal/redeal.py#L347
    # 统计双明手东家拿到的墩数
    lead_payoff.add_data(deal.dd_all_tricks('C', 'E'))

print("模拟分析结果 (xx)为上下误差:", found)

lead_payoff.report()
