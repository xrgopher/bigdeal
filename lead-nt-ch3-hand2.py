#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from redeal import *
from redeal import dds
from redeal.global_defs import Seat, Suit, Card, Rank, Strain

# https://ebooksbridge.com/www/ebb/index.php?main_page=ebb_product_book_info&products_id=456
# 致胜无将首攻 (Winning Notrump Leads)
# - Chapter 1, hand 3

# 这是为了显示，看看首攻
bid="""
叫牌: (双无)
  西   北   东   南
       Pass Pass 1NT
  Pass 3NT
你坐西，如何找到最佳首攻？
假设
 南: 15-17点，5332，4432，4333
 北：9-11点，黑桃红桃小于4张，因为没有转移叫和斯台曼 (如果北家点数再高，击败可能性很低不用考虑)
"""

def accept(deal):
    north, south = deal.north, deal.south
    return (
        balanced(south) and       # 南家牌型 
        15 <= south.hcp <= 17 and # 南家点力
        len(north.spades) <4 and len(north.hearts) < 4 and # 北家牌型
        9 <= north.hcp <= 11                               # 北家点力
    )

sample = []
N_TRIES=100
def nt_leader(predeal, del_lst=[], n_tries=N_TRIES):
    dealer = Deal.prepare(predeal)

    # 南北牌的模拟(和新睿的不一定一致)
    # 南: 15-17点，5332，4432，4333
    # 北：9-11点，黑桃红桃小于4张，因为没有转移叫和斯台曼

    # https://github.com/anntzer/redeal#shape
    # 牌型设置
    balanced = Shape("(4333)") + Shape("(4432)") + Shape("(5332)")



    simulation = OpeningLeadSim(accept, "3NS", imps)
    simulation.initial(dealer)

    found = 0
    for _ in range(1000*n_tries):
        if found >= n_tries:
            break
        deal = dealer()
        if not accept(deal):
            continue

        found += 1
        score_3NS = deal.dd_score('3NS')
        if score_3NS < 0:
            # 代表打宕了，因为南北负分，记录下来
            sample.append(deal)
        simulation.do(deal)

    # 从结果里去掉不需要的牌
    # check https://github.com/anntzer/redeal/blob/master/redeal/redeal.py#L623
    for card in del_lst:
        simulation.payoff.entries.remove(Card.from_str(card))
    
    print("模拟分析结果: ", found, " 副牌")
    simulation.final(n_tries)

print("致胜无将首攻 - 第三章第二副")
print(bid)


predeal = {"W": "92 74 AK7542 964"}
print("你坐西，对抗3NT，拿牌如下，如何首攻:", predeal['W'])

cards_needs_removed=["S2", "H4","C9","C4"]
nt_leader(predeal,cards_needs_removed)

# 然后检测数据
deal = sample[0]
Deal.set_str_style('long')
print("3NT牌例:", deal)

result=deal.dd_all_tricks("N" , "W")
score_3NS = deal.dd_score('3NS')
print("无局拿到最多的分数", score_3NS)
print("然后看看各个首攻的墩数:")
for key in result:
    print(key, result[key])
print("为啥没有DK？")
#print(result)