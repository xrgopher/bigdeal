# 简介

bridge analyzer using deal generator and dds

```mermaid
flowchart TB
 A-.->B
   C-. text .-> D

```


# 安装

## Windows

* install python 3.6+ https://www.python.org/downloads/windows/ 
* download [redeal-master.zip](redeal-master.zip)

````
python -mpip install --user --upgrade redeal-master.zip
# change bid.py n=2000 => n=100
python bid.py
````

## Mac

* install python 3.6+ https://www.python.org/downloads/mac-osx/

````
python3 -mpip install --user --upgrade git+https://github.com/anntzer/redeal 
pip3 install git+https://github.com/anntzer/redeal target='path' # You have to designate a path else it will be some errors when import this package)//Keep Internet Connected! It takes some time to clone from webpage. It's better to upgrade pip and init git.
# change bid.py n=2000 => n=100
python3 bid.py
````

### 常见问题

* Mac: hang there during installation (github is ok to connect) // fixed

# 每周任务 

## 第一周任务：环境和发牌器bid.py

熟悉环境，理解现有的[bid.py](bid.py)发牌程序，并能根据老师的指示准确设定东家的牌（阻击3S的例子），配合老师一起探讨出正确的结果。学有余力的同学学习网上编辑、redeal的说明..

根据微信的文章[【惊讶反转】大样本分析结果表明，3NT的成功率为64%，而5C/D的机会高达](https://mp.weixin.qq.com/s?__biz=MzAwNzA2NTU1NQ==&mid=2648781917&idx=1&sn=93ca8aba9e5cf0247b8565806a2709fd)来改造[bid.py](bid.py)如下的代码片段

````python
def accept(deal):
    if deal.east.hcp <5 or deal.east.hcp >8:
        return False
    if len(deal.east.spades) != 7:
        return False
    if len(deal.east.hearts) > 4:
        return False
    if len(deal.east.clubs) > 4:
        return False
    if len(deal.east.diamonds) > 4:
        return False
    if deal.west.hcp >18:
        return False
    return True
````

@xjtuzhu 给出了条件

> 点力就写五到十吧
H可以是XXXX
C和D可以是五张，但点力的60%要在S中


> 七张的时候，S质量要好
八张可以稍微差点

## 第二周任务：分析首攻lead.py

结合致胜首攻一书，有老师挑一到两个牌例，编出程序，调试出和书上一样的结果。学有余力的争取能独立完成网上发布

理解现有的[lead.py](lead.py)，能够独立运行，再更新在各自的程序里`lead-xxx.py`，在网页里查看结果 [https://xrgopher.gitlab.io/bigdeal/](https://xrgopher.gitlab.io/bigdeal/)

然后找一副致胜无将一书里的牌例进行分析，期望得到相同结果

# 建议和思考

@坚心

我觉得模拟的不是特别靠谱是可能有两个原因

1. 一是数据量
2. 二是未知牌的条件设置并不是完全像程序设置的那么死

这个程序最终版本我觉得应该是一个可视化界面，然后另外三家牌每门花色都有张数点力限制，总的还有点力限制，大概7个条件，然后三家牌，再乘以3。而且这个发牌程序里，如果需要100副符合条件的牌，会发100*n的牌，然后里面选100副符合要求的牌，再看这100副里各个定约的结果。

# 目标

## 第一周任务：环境和发牌器bid.py

熟悉环境，理解现有的bid.py发牌程序，并能根据老师的指示准确设定东家的牌（阻击3S的例子），配合老师一起探讨出正确的结果。学有余力的同学学习网上编辑、redeal的说明..

## 第二周任务：分析首攻lead.py

结合致胜首攻一书，有老师挑一到两个牌例，编出程序，调试出和书上一样的结果。学有余力的争取能独立完成网上发布

## 第三周任务：实战演练

根据老师提供的叫牌和首攻牌例，配合分析，希望能达到实战效果，可以的话可以接单子干活。学有余力的同学，我会辅导建立自己的网站和整套系统（单飞了）

## 第四周任务：总结讨论

这周该毕业了，争取每人写一篇不同牌例的分析文章（可以让群里老师评比），最佳者我再送一套桥牌书！总结讨论这个活动如何继续...

# 致谢

@坚心 @自由最重要 @tutulfy

# More deals

[群组IMP赛 20201210 牌号 1/8](http://www.xinruibridge.com/deallog/DealLog.html?bidlog=P,P%3B1S,P,2S,P%3B4S,P,P,P%3B&playlog=W:8H,2H,JH,QH%3BS:AS,5S,2S,TS%3BS:KS,7S,3S,2D%3BS:8S,9S,JS,3D%3BN:KH,AH,3H,5H%3BE:8C,KC,AC,4C%3BW:AD,6D,7D,TD%3BW:3C,5C,QC,9C%3BE:&deal=J632.K42.K96.754%20T.AJ6.Q87532.QT8%20AKQ84.QT973.T.K9%20975.85.AJ4.AJ632&vul=None&dealer=N&contract=4S&declarer=S&wintrick=9&score=-50&str=%E7%BE%A4%E7%BB%84IMP%E8%B5%9B%2020201210%20%E7%89%8C%E5%8F%B7%201/8&dealid=995494867&pbnid=345494765)  @xjtuzhu

# Reference

* [https://xrgopher.gitlab.io/bigdeal/](https://xrgopher.gitlab.io/bigdeal/)
* https://github.com/anntzer/redeal
* [A Simulation Tutorial for Better Decisionmaking at Bridge](http://datadaydreams.com/posts/a-simulation-tutorial-for-better-decisionmaking-at-bridge/)
* [【惊讶反转】大样本分析结果表明，3NT的成功率为64%，而5C/D的机会高达](https://mp.weixin.qq.com/s?__biz=MzAwNzA2NTU1NQ==&mid=2648781917&idx=1&sn=93ca8aba9e5cf0247b8565806a2709fd)
* [Stayman or Pass?](http://www.rpbridge.net/8z15.htm), related code: [pavlicek_8z15.py](https://github.com/anntzer/redeal/blob/master/examples/pavlicek_8z15.py)

## 关于机器学习和桥牌

* https://www.milestre.nl/blog/blogitem/milestre-blog/2020/03/11/predict-a-bidding-in-a-bridge-card-game-with-machine-learning-.net-(ml.net)
* https://www.youtube.com/watch?v=CRBNI8UdHhE Deep Neural Networks for Double Dummy at Bridge - Lorand Dali
* https://www.quora.com/Can-AI-play-perfect-Bridge 
* https://privat.bahnhof.se/wb758135/bridge/history.html DDS
* https://www.csie.ntu.edu.tw/~htlin/paper/doc/bridgedrl.pdf
* https://hal.archives-ouvertes.fr/hal-01665867/document about AI for wbridge5
* https://realworld-sdm.github.io/paper/42.pdf Simple is Better: Training an End-to-end Contract Bridge Bidding Agent
without Human Knowledge

## 国内论文

* http://gb.oversea.cnki.net/KCMS/detail/detailall.aspx?filename=1019047137.nh&dbcode=CMFD&dbname=CMFDTEMP 
* https://cloud.tencent.com/developer/article/1448307 
