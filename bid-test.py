#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from redeal import *
from redeal import dds
from redeal.global_defs import Seat, Suit, Card, Rank, Strain

def accept(deal):
    e = deal.east
    if e.hcp <5 or e.hcp >10:
        return False
    if len(e.spades) < 7:
        return False
    print(deal.east.spades)
    print(deal.north.spades)
    if '5' not in str(e.spades):
        return False
    return True

predeal = {Seat['N']: H('AK52 8 AK97 A976')}
dealer = Deal.prepare(predeal)
deal = dealer(accept, tries=1000000)

print(deal.east)