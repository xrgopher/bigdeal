#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from redeal import *
from redeal import dds
from redeal.global_defs import Seat, Suit, Card, Rank, Strain
import math

Deal.set_str_style('long')

Hand.set_str_style('short')

vuln = True
predeal = {Seat['N']: H('AK52 8 AK97 A976')}
dealer = Deal.prepare(predeal)

bid="""
Bidding: (N/S)
  W   N   E   S
          3S  P
  P   ?
"""

print(bid)
print("Sit in north:" , predeal[Seat['N']])

w2q = Evaluator(4, 3, 2, 1)

def accept(deal):
    if deal.east.hcp <5 or deal.east.hcp >10:
        return False
    if w2q(deal.east.spades) < 0.6*deal.east.hcp:
        return False
    if len(deal.east.spades) < 7 or len(deal.east.spades)>8:
        return False
    if len(deal.east.spades) == 7 and w2q(deal.east.spades)<4:#黑桃套太破
        return False
    if len(deal.east.hearts) > 4 and w2q(deal.east.hearts)>0:#红心有点且有四张
        return False
    if len(deal.east.clubs) > 5:
        return False
    if len(deal.east.diamonds) > 5:
        return False
    if deal.west.hcp >18:
        return False
    return True

imps_payoff = Payoff(('3S', '3NT', '4H','5C','5D'), imps)

found = 0
n = 500
numof3s = numof3n = numof4h = numof5c = numof5d = 0
deal_1 = deal_2 = deal_3 = deal_4 =deal_5= None
for _ in range(10000 * n):
    if found > n:
        break
    deal = dealer()
    if not accept(deal):
        continue
    found += 1
    if found%100 == 0:
        print(found)
    score_3s = deal.dd_score('3SE', vuln)
    score_3n = deal.dd_score('3NN', vuln)
    score_4h = deal.dd_score('4HN', vuln)
    score_5c = deal.dd_score('5CS', vuln)
    score_5d = deal.dd_score('5DS', vuln)

    if score_3s > 0:
        deal_1 = deal
        numof3s+=1#3s完成数量+1
    if score_3n > 0:
        deal_2 = deal
        numof3n+=1
    if score_4h > 0:
        deal_3 = deal
        numof4h+=1
    if score_5c > 0:
        numof5c+=1
        deal_4 = deal
    if score_5d > 0:
        numof5d+=1
        deal_5 = deal

    data = {
        '3NT': score_3n,
        '4H': score_4h,
        '3S': score_3s,
        '5C': score_5c,
        '5D': score_5d

    }
    imps_payoff.add_data(data)

print("模拟分析结果:", found)

imps_payoff.report()

print("3S的牌例:", deal_1)
print("3NT牌例:", deal_2)
print("4H牌例:", deal_3)
print("5C牌例:", deal_4)
print("5D牌例:", deal_5)
print("3S定约完成数量:",numof3s,"占比:",numof3s/found*100,'%')#计算定约占总发牌数的比例
print("3NT定约完成数量:",numof3n,"占比:",numof3n/found*100,'%')
print("4H定约完成数量:",numof4h,"占比:",numof4h/found*100,'%')
print("5C定约完成数量:",numof5c,"占比:",numof5c/found*100,'%')
print("5D定约完成数量:",numof5d,"占比:",numof5d/found*100,'%')
