#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from redeal import *
from redeal import dds
from redeal.global_defs import Seat, Suit, Card, Rank, Strain

Deal.set_str_style('long')

Hand.set_str_style('short')

vuln = True
predeal = {Seat['N']: H('AK52 8 AK97 A976')}
dealer = Deal.prepare(predeal)

bid="""
Bidding: (N/S)
  W   N   E   S
          3S  P
  P   ?
"""

print(bid)
print("Sit in north:" , predeal[Seat['N']])

def accept(deal):
    if deal.east.hcp <5 or deal.east.hcp >8:
        return False
    if len(deal.east.spades) != 7:
        return False
    if len(deal.east.hearts) > 4:
        return False
    if len(deal.east.clubs) > 4:
        return False
    if len(deal.east.diamonds) > 4:
        return False
    if deal.west.hcp >18:
        return False
    return True

imps_payoff = Payoff(('3S', '3NT', '5C','5D'), imps)

found = 0
n = 1000

deal_1 = deal_2 = deal_3 = deal_4 = None
for _ in range(5000 * n):
    if found > n:
        break
    deal = dealer()
    if not accept(deal):
        continue
    found += 1
    score_3s = deal.dd_score('3SE', vuln)
    score_3n = deal.dd_score('3NN', vuln)
    score_5c = deal.dd_score('5CS', vuln)
    score_5d = deal.dd_score('5DS', vuln)


    if score_3s > 0:
        deal_1 = deal
    if score_3n > 0:
        deal_2 = deal
    if score_5c > 0:
        deal_3 = deal
    if score_5d > 0:
        deal_4 = deal
    data = {
        '3NT': score_3n,
        '3S': score_3s,
        '5C': score_5c,
        '5D': score_5d
    }
    imps_payoff.add_data(data)
    
print("模拟分析结果:", str(found))

imps_payoff.report()

print("3S的牌例:", deal_1)
print("3NT牌例:", deal_2)
print("5C牌例:", deal_3)
print("5D牌例:", deal_4)
